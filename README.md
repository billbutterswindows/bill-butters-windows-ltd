Bill Butters is an established and reputable family-run business based in Sherborne, Dorset. We specialise in the design, manufacture, supply and installation of high quality yet affordable PVC-u windows and aluminium products. We offer a very personal and friendly service second to none.

Address: South Western Business Park, Unit 1A, Sherborne, Dorset DT9 3PS, UK

Phone: +44 1935 816168
